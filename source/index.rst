.. ChessTM documentation master file, created by
   sphinx-quickstart on Tue Apr 19 14:32:22 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ChessTM's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   concept
   tests

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

